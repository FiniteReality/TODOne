using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TODOne.Attributes;

namespace TODOne.Models
{
    public class Chore
    {
        public ulong ChoreId { get; set; }
        public ApplicationUser User { get; set; }
        [MaxLength(50)]
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime DueDate { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public DateTime SetDate { get; set; }
        [Timestamp]
        public DateTime LastUpdated { get; set; }

        public ChoreDTO ToDTO()
        {
            return new ChoreDTO
            {
                Id = ChoreId,
                Name = Name,
                Description = Description,
                DueDate = DueDate,
                SetDate = SetDate,
                LastUpdated = LastUpdated
            };
        }
    }

    public class ChoreDTO
    {
        public ulong Id { get; set; }
        [MaxLength(50)]
        public string Name { get; set; }
        public string Description { get; set; }
        [GreaterThan(nameof(SetDate))]
        [DataType(DataType.DateTime)]
        public DateTime DueDate { get; set; }
        [DataType(DataType.DateTime)]
        public DateTime SetDate { get; set; }
        public DateTime LastUpdated { get; set; }
    }
}