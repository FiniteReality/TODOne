using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;

namespace TODOne.Models
{
    public class ApplicationUser : IdentityUser
    {
        public IList<Chore> Chores { get; set; }
    }
}