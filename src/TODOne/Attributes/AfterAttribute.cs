using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.ModelBinding.Validation;

namespace TODOne.Attributes
{
    public class GreaterThanAttribute : CompareAttribute
    {
        public GreaterThanAttribute(string otherProperty)
            : base(otherProperty)
        {
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var prop = validationContext.ObjectType.GetProperty(OtherProperty);
            if (prop == null)
                return new ValidationResult($"Could not find property {OtherProperty}");
            
            var otherValue = prop.GetValue(validationContext.ObjectInstance);

            int compare = 0;
            if (value is IComparable comparable)
                compare = comparable.CompareTo(otherValue);
            else
                return new ValidationResult($"Could not compare value with {OtherProperty}");

            if (compare == 1)
                return ValidationResult.Success;
            else
                return new ValidationResult($"The value must be smaller than {OtherProperty}");
        }
    }
}