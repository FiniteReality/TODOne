using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TODOne.Models;

namespace TODOne.Services
{
    public class ChoreService
    {
        private readonly ApplicationDbContext _dbContext;

        public ChoreService(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }


        public async Task<Chore> AddChoreAsync(ApplicationUser user, Action<Chore> config = null)
        {
            var chore = new Chore();

            if (config != null)
                config(chore);

            chore.User = user;

            _dbContext.Chores.Add(chore);
            await _dbContext.SaveChangesAsync();

            return chore;
        }

        public IQueryable<Chore> GetChores(ApplicationUser user)
        {
            return _dbContext.Chores.Where(x => x.User == user);
        }

        public Chore GetChore(ulong id)
        {
            return _dbContext.Chores.SingleOrDefault(x => x.ChoreId == id);
        }
    }
}