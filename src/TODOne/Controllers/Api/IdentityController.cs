using System.Linq;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace TODOne.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class IdentityController : Controller
    {
        [HttpGet("[action]")]
        public IActionResult Get()
        {
            return new JsonResult(
                User.Claims.Select(x => new {x.Type, x.Value})
            );
        }
    }
}