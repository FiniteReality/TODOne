using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using TODOne.Models;
using TODOne.Services;

namespace TODOne.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class ChoresController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly ChoreService _chores;

        public ChoresController(
            UserManager<ApplicationUser> userManager,
            ChoreService chores)
        {
            _userManager = userManager;
            _chores = chores;
        }

        [HttpGet("[action]")]
        public async Task<IEnumerable<ChoreDTO>> All()
        {
            var user = await _userManager.GetUserAsync(User);
            var chores = _chores.GetChores(user);

            return chores.Select(x => x.ToDTO());
        }

        [HttpPost("[action]")]
        public async Task<ActionResult> Create([FromBody]ChoreDTO info)
        {
            // Ensure the chore is valid, and return a bad request if it isn't
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            // Create a new chore for the given user
            var user = await _userManager.GetUserAsync(User);
            var chore = await _chores.AddChoreAsync(user, c =>
            {
                c.Name = info.Name;
                c.Description = info.Description;
                c.DueDate = info.DueDate;
                c.SetDate = info.SetDate;
            });

            // Return the info from the DB so we can format it cleanly
            return Json(chore.ToDTO());
        }
    }
}
