using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using TODOne.Models;

namespace TODOne.Controllers
{
    [Route("api/[controller]")]
    [Authorize]
    public class AccountController : Controller
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;

        public AccountController(
            UserManager<ApplicationUser> userManager,
            SignInManager<ApplicationUser> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }

        [HttpGet("[action]")]
        public Task<ApplicationUser> Get()
        {
            return _userManager.GetUserAsync(User);
        }

        [HttpPost("[action]")]
        [AllowAnonymous]
        public async Task<ActionResult> Create([FromBody]UserInformation info)
        {
            // Ensure the user's request is valid, and return a http error if not
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var user = new ApplicationUser
            {
                UserName = info.Username,
                Email = info.EMail
            };

            var result = await _userManager.CreateAsync(user, info.Password);

            if (!result.Succeeded)
            {
                foreach (var error in result.Errors)
                {
                    // TODO: this should be made more sturdy
                    if (error.Code.StartsWith("Password"))
                        ModelState.AddModelError("Password", error.Description);
                    else
                        ModelState.AddModelError("Username", error.Description);
                }

                // TODO: Is BadRequest correct to return here?
                return BadRequest(ModelState);
            }

            await _signInManager.SignInAsync(user,
                isPersistent: false);

            // If we got to this point, the user has been created successfully.
            return NoContent();
        }

        public class UserInformation
        {
            [MaxLength(20)]
            [Required(ErrorMessage = "A user name is required")]
            public string Username { get; set; }

            [Required(ErrorMessage = "A password is required")]
            public string Password { get; set; }

            [Required(ErrorMessage = "An E-Mail address is required")]
            [EmailAddress]
            public string EMail { get; set; }
        }
    }
}
