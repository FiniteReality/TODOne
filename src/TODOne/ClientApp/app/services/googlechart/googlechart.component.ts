﻿import { Injectable, OnInit } from '@angular/core';
declare var google: any;

export interface GoogleChartComponent {
    draw(): void;
}

@Injectable()
export class GoogleChartService {
    private static loaded: boolean;
    private static charts: GoogleChartComponent[] = [];

    static initialize() {
        if (google && !this.loaded) {
            console.log('loading google charts');
            google.charts.load('current', { packages: ['gantt', 'timeline'] });
            google.charts.setOnLoadCallback(this.drawCharts);
            this.loaded = true;
        }
    }

    private static drawCharts(): void {
        for (const chart of GoogleChartService.charts) {
            chart.draw();
        }
    }

    public addChart(component: GoogleChartComponent): void {
        GoogleChartService.charts.push(component);
    }

    public removeChart(component: GoogleChartComponent): void {
        let index = GoogleChartService.charts.indexOf(component);
        GoogleChartService.charts.splice(index, 1);
    }
}

GoogleChartService.initialize();