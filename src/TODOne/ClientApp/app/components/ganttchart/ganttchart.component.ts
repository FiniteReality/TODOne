import { Component } from '@angular/core';
import { GoogleChartComponent, GoogleChartService } from '../../services/googlechart/googlechart.component';
import { OnInit, OnDestroy } from '@angular/core';
import { Input } from '@angular/core';
import { ViewContainerRef } from '@angular/core';

declare var google: any;

export interface GanttChartData {
    taskId: string;
    taskName: string;
    resource: string|null;
    start: Date|null;
    end: Date|null;
    durationMs: number|null;
    percentComplete: number;
    dependencies: string[]|null;
}

@Component({
    selector: 'gantt-chart',
    inputs: ['columns'],
    templateUrl: './ganttchart.component.html',
    styleUrls: ['./ganttchart.component.css']
})
export class GanttChartComponent implements GoogleChartComponent, OnInit, OnDestroy{
    private _charts: GoogleChartService;
    private _viewContainer: ViewContainerRef;

    @Input() columns: GanttChartData[];
    @Input() options: any;

    constructor(public charts: GoogleChartService, public viewContainer: ViewContainerRef) {
        this._charts = charts;
        this._viewContainer = viewContainer;
    }

    ngOnDestroy(): void {
        throw new Error("Method not implemented.");
    }
    ngOnInit(): void {
        this._charts.addChart(this);
    }

    draw(): void {
        if (this._viewContainer.element.nativeElement) {
            let chart = new google.visualization.Gantt(this._viewContainer.element.nativeElement);
            let data = new google.visualization.DataTable({
                cols: [
                    { id: 'Task ID', type: 'string' },
                    { id: 'Task Name', type: 'string' },
                    { id: 'Resource', type: 'string' },
                    { id: 'Start', type: 'date' },
                    { id: 'End', type: 'date' },
                    { id: 'Duration', type: 'number' },
                    { id: 'Percent Complete', type: 'number' },
                    { id: 'Dependencies', type: 'string' }
                ]
            });

            for (const row of this.columns) {
                data.addRow(
                    [row.taskId, row.taskName, row.resource,
                        row.start, row.end, row.durationMs,
                        row.percentComplete || 0,
                        row.dependencies ? row.dependencies.join(',') : null]
                )
            }

            chart.draw(data, this.options);
        }
    }
}
