import { Component } from '@angular/core';

@Component({
    selector: 'home',
    templateUrl: './home.component.html'
})
export class HomeComponent {
    data: any = [
        {
            taskId: 'wakeUp',
            taskName: 'Wake up',
            durationMs: 1000,
            percentComplete: 50
        },
        {
            taskId: 'skipSchool',
            taskName: 'Skip School',
            durationMs: 1500,
            dependencies: ['wakeUp']
        },
        {
            taskId: 'turnOnAtari',
            taskName: 'Turn on the Atari',
            durationMs: 800,
            dependencies: ['wakeUp', 'skipSchool']
        },
        {
            taskId: 'playGames',
            taskName: 'Play Games',
            durationMs: 3000,
            dependencies: ['turnOnAtari']
        }
    ];
}
